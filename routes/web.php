<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Auth::routes();

////GENRE START
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/genre', 'GenreController@index')->name('genre');

//save genre
Route::post('genreRequest', ['as' => 'genreRequest', 'uses' => 'GenreController@store']);
//search genre
Route::get('genreSearch', ['as' => 'genreSearch', 'uses' => 'GenreController@search']);
//edit genre
Route::get('/genre/edit/{id}','GenreController@edit');
//update genre
Route::post('/genre/update/{id}','GenreController@update');
//delete genre
Route::post('/genre/destroy/{id}','GenreController@destroy');
//suggest genre
Route::get('/genre/suggest','GenreController@suggest');

///GENRE END

///SECTION START
Route::get('/section', 'SectionController@index')->name('section');

Route::post('sectionRequest', ['as' => 'sectionRequest', 'uses' => 'SectionController@store']);
//search section
Route::get('sectionSearch', ['as' => 'sectionSearch', 'uses' => 'SectionController@search']);
//edit section
Route::get('/section/edit/{id}','SectionController@edit');
//update section
Route::post('/section/update/{id}','SectionController@update');
//delete section
Route::post('/section/destroy/{id}','SectionController@destroy');
//suggest section
Route::get('/section/suggest','SectionController@suggest');
///SECTION END


///CLIENT START
Route::get('/client', 'ClientController@index')->name('client');

Route::post('clientRequest', ['as' => 'clientRequest', 'uses' => 'ClientController@store']);
//search client
Route::get('clientSearch', ['as' => 'clientSearch', 'uses' => 'ClientController@search']);
//edit client
Route::get('/client/edit/{id}','ClientController@edit');
//update client
Route::post('/client/update/{id}','ClientController@update');
//delete client
Route::post('/client/destroy/{id}','ClientController@destroy');
//suggest client
Route::get('/client/suggest','ClientController@suggest');
///CLIENT END


///BOOK START
Route::get('/book', 'BookController@index')->name('book');

Route::post('bookRequest', ['as' => 'bookRequest', 'uses' => 'BookController@store']);
//search book
Route::get('bookSearch', ['as' => 'bookSearch', 'uses' => 'BookController@search']);
//edit book
Route::get('/book/edit/{id}','BookController@edit');
//update book
Route::post('/book/update/{id}','BookController@update');
//delete book
Route::post('/book/destroy/{id}','BookController@destroy');
//view book
Route::get('/book/show/{id}','BookController@show');
//borrow book
Route::post('bookBorrow', ['as' => 'bookBorrow', 'uses' => 'BookController@borrow']);
///BOOK END


///TRANSACTION START
Route::get('/transaction', 'TransactionController@index')->name('transaction');
//search transaction
Route::get('transactionSearch', ['as' => 'transactionSearch', 'uses' => 'TransactionController@search']);
//return transaction
Route::post('/transaction/returnBooks/{id}','TransactionController@returnBooks');
///TRANSACTION END
// Route::post('/genre/post','GenreController@store');
