<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    protected $fillable = [
    	'book_name',
    	'author_name',
    ];

    public function genre(){
    	return $this->belongsTo('App\Genre','genre_id');
    }

    public function section(){
    	return $this->belongsTo('App\Section','section_id');
    }

    // public function transaction(){
    // 	return $this->hasOne('App\T');
    // }

    public function search($search){
    	// $articles =DB::table('books')
     //            ->join('genres', 'genres.genre_id', '=', 'books.id')
     //            ->join('sections', 'sections.section_id', '=', 'books.user_id')
     //            ->select('books.*','articles.title','articles.body','users.username', 'category.name')
     //            ->get();
    }
}
