<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\Input;
use DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
        $clients = Client::where('is_deleted',0)->orderBy('created_at','DESC')->paginate(10);

        return view('client.index',compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email'
        ]);

        $client = new Client();
        $client->firstname   = $request->firstname;
        $client->lastname   = $request->lastname;
        $client->email   = $request->email;
        $client->save();

        $this->ajaxResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $client = Client::find($id);
        echo json_encode($client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email'
        ]);

        $client = Client::find($id);
        $client->firstname = $request->get('firstname');
        $client->lastname = $request->get('lastname');
        $client->email = $request->get('email');
        $client->save();

        $this->ajaxResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $client = Client::find($id);
        $client->is_deleted = 1;
        $client->save();

        $this->ajaxResponse();
    }

    public function ajaxResponse(){
        $clients = Client::where('is_deleted',0)->orderBy('created_at','DESC')->paginate(10);

        $data['success'] = 1;
        $data['view'] = view('client.table',compact('clients'))->render();
        echo json_encode($data);
    }

    public function search(Request $request)
    {
        $search =  Input::get('search');
                            
        $clients = Client::where(function($query) use ($search){
                                $query->where('firstname', 'LIKE', '%' . $search . '%');
                                $query->orWhere('lastname', 'LIKE', '%' . $search . '%');
                                $query->orWhere('email', 'LIKE', '%' . $search . '%');
                            })
                            ->where('is_deleted',0)                            
                            ->orderBy('created_at','DESC')->paginate(10);

        $clients->appends(['search' => $search]);

        if($request->ajax()){
            $data['data'] = $clients;
            $data['success'] = 1;
            $data['view'] = view('client.table',compact('clients'))->render();
            echo json_encode($data);
        }else{
            return view('client.index',compact('clients'));
        }
        
    }

    //suggest client
    public function suggest(){
        $search =  Input::get('q');
        $clients = Client::where('is_deleted',0)->where('firstname', 'LIKE', '%' . $search . '%')->orWhere('lastname', 'LIKE', '%' . $search . '%')->orderBy('created_at','DESC')->get();
        $formatted_data = array();
        foreach ($clients as $key => $client) {
            array_push($formatted_data,array('id'=>$client->id,'text'=>$client->firstname." ".$client->lastname));
        }

        echo json_encode($formatted_data);
    }
}
