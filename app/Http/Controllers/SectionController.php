<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;
use Illuminate\Support\Facades\Input;
use DB;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
        $sections = Section::where('is_deleted',0)->orderBy('created_at','DESC')->paginate(10);

        return view('section.index',compact('sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $section = new Section();
        $section->section_name   = $request->section_name;
        $section->save();

        $this->ajaxResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $section = Section::find($id);
        echo json_encode($section);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'section_name' => 'required'
        ]);

        $section = Section::find($id);
        $section->section_name = $request->get('section_name');
        $section->save();

        $this->ajaxResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $section = Section::find($id);
        $section->is_deleted = 1;
        $section->save();

        $this->ajaxResponse();
    }

    //return ajax response
    public function ajaxResponse(){
        $sections = Section::where('is_deleted',0)->orderBy('created_at','DESC')->paginate(10);

        $data['success'] = 1;
        $data['view'] = view('section.table',compact('sections'))->render();
        echo json_encode($data);
    }

    //searching
    public function search(Request $request)
    {
        $search =  Input::get('search');
        $sections = Section::where('is_deleted',0)->where('section_name', 'LIKE', '%' . $search . '%')->orderBy('created_at','DESC')->paginate(10);

        $sections->appends(['search' => $search]);

        if($request->ajax()){
            $data['data'] = $sections;
            $data['success'] = 1;
            $data['view'] = view('section.table',compact('sections'))->render();
            echo json_encode($data);
        }else{
            return view('section.index',compact('sections'));
        }
        
    }

    //suggest section
    public function suggest(){
        $search =  Input::get('q');
        $sections = Section::where('is_deleted',0)->where('is_deleted',0)->where('section_name', 'LIKE', '%' . $search . '%')->orderBy('created_at','DESC')->get();
        $formatted_data = array();
        foreach ($sections as $key => $section) {
            array_push($formatted_data,array('id'=>$section->id,'text'=>$section->section_name));
        }

        echo json_encode($formatted_data);
    }
}
