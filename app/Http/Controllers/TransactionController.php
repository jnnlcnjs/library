<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Transaction;
use Illuminate\Support\Facades\Input;
use DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
        $transactions = Transaction::orderBy('created_at','DESC')->with(['book','book.section','book.genre','client'])->paginate(10);
        // echo json_encode($transactions);die();
        return view('transaction.index',compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $transaction = Transaction::find($id);
        $transaction->delete();

        $this->ajaxResponse();
    }

    //search transaction
    public function search(Request $request)
    {
        $search =  Input::get('search');
        $transactions = Transaction::with(['book','book.section','book.genre','client'])
                            ->where(function($query) use ($search){
                                $query->whereHas('book',function($q) use ($search){
                                    $q->where('book_name','LIKE','%'.$search.'%');//check if book name exist 
                                    $q->orWhere('author_name','LIKE','%'.$search.'%');//check if author exist 
                                });
                                $query->orWhereHas('book.genre',function($q) use ($search){
                                    $q->where('genre_name','LIKE','%'.$search.'%');//check if genre name exist 
                                });
                                $query->orWhereHas('book.section',function($q) use ($search){
                                    $q->where('section_name','LIKE','%'.$search.'%');//check if section name exist
                                });
                                $query->orWhereHas('client',function($q) use ($search){
                                    $q->where('firstname','LIKE','%'.$search.'%');//check if first name exist
                                    $q->orWhere('lastname','LIKE','%'.$search.'%');//check if last name exist
                                });
                            })
                            ->orderBy('created_at','DESC')->paginate(10);
                            

        $transactions->appends(['search' => $search]);

        if($request->ajax()){
            $data['data'] = $transactions;
            $data['success'] = 1;
            $data['view'] = view('transaction.table',compact('transactions'))->render();
            echo json_encode($data);
        }else{
            return view('transaction.index',compact('transactions'));
        }
        
    }

    //return of books
    public function returnBooks($id){
        $transactions = Transaction::find($id);
        $transactions->return_at = date('Y-m-d H:i:s');
        $transactions->save();

        $book = Book::find($transactions->book_id);
        $book->transaction_id = 0;
        $book->is_borrowed = 0;
        $book->save();

        $this->ajaxResponse();
    }

    //ajax response
    public function ajaxResponse(){
        $transactions = Transaction::orderBy('created_at','DESC')->with(['book','book.section','book.genre','client'])->paginate(10);

        $data['success'] = 1;
        $data['view'] = view('transaction.table',compact('transactions'))->render();
        echo json_encode($data);
    }
}
