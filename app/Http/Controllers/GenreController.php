<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use Illuminate\Support\Facades\Input;
use DB;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        
        // $genres = Genre::all();
        // $genres = Genre::orderBy('created_at','DESC')->limit(10)->get();
        // $genres = Genre::orderBy('created_at','DESC')->limit(10)->paginate(10);
        $genres = Genre::where('is_deleted',0)->orderBy('created_at','DESC')->paginate(10);


        return view('genre.index',compact('genres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $genre = new Genre();
        $genre->genre_name   = $request->genre_name;
        $genre->save();

        $this->ajaxResponse();
        // return response()->json(['success'=>'Data is successfully added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = Genre::find($id);
        echo json_encode($genre);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $genre = Genre::find($id);

        $this->validate($request, [
            'genre_name' => 'required'
        ]);

        $genre = Genre::find($id);
        $genre->genre_name = $request->get('genre_name');
        $genre->save();

        $this->ajaxResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $genre = Genre::find($id);
        $genre->is_deleted = 1;
        $genre->save();

        $this->ajaxResponse();
    }

    public function ajaxResponse(){
        // $genres = Genre::limit(10)->get();
        // $genres = Genre::orderBy('created_at','DESC')->limit(10)->get();
        $genres = Genre::where('is_deleted',0)->orderBy('created_at','DESC')->paginate(10);

        $data['success'] = 1;
        $data['view'] = view('genre.table',compact('genres'))->render();
        // echo "<pre>";print_r($data);die();
        echo json_encode($data);
        // return response()->json(['success'=>'Data is successfully added','data'=>$data]);
        // echo json_encode($data);
    }

    public function search(Request $request)
    {
        $search =  Input::get('search');
        $genres = Genre::where('is_deleted',0)->where('genre_name', 'LIKE', '%' . $search . '%')->orderBy('created_at','DESC')->paginate(10);

        // DB::enableQueryLog();

        // # your laravel query builder goes here

        // $laQuery = DB::getQueryLog();
        // echo "<pre>";print_r($laQuery);die();

        $genres->appends(['search' => $search]);

        if($request->ajax()){
            $data['data'] = $genres;
            $data['success'] = 1;
            $data['view'] = view('genre.table',compact('genres'))->render();
            echo json_encode($data);
        }else{
            return view('genre.index',compact('genres'));
        }
        
    }
    //suggest genre
    public function suggest(){
        $search =  Input::get('q');
        $genres = Genre::where('genre_name', 'LIKE', '%' . $search . '%')->orderBy('created_at','DESC')->get();
        // echo json_encode($genres);
        $formatted_data = array();
        foreach ($genres as $key => $genre) {
            array_push($formatted_data,array('id'=>$genre->id,'text'=>$genre->genre_name));
        }

        echo json_encode($formatted_data);
    }
}
