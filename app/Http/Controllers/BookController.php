<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Transaction;
use Illuminate\Support\Facades\Input;
use DB;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        //
        $books = Book::where('is_borrowed',0)->where('is_deleted',0)->orderBy('created_at','DESC')->with(['genre','section'])->paginate(10);
        // echo json_encode($books);die();
        return view('book.index',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'book_name' => 'required',
            'author_name' => 'required',
            'section_id' => 'required',
            'genre_id' => 'required',
        ]);

        $book = new Book();
        $book->book_name   = $request->book_name;
        $book->author_name   = $request->author_name;
        $book->section_id   = $request->section_id;
        $book->genre_id   = $request->genre_id;
        $book->save();

        $this->ajaxResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $book = Book::with(['genre','section'])->find($id);
        echo json_encode($book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $book = Book::with(['genre','section'])->find($id);
        echo json_encode($book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'book_name' => 'required',
            'author_name' => 'required',
            'section_id' => 'required',
            'genre_id' => 'required',
        ]);

        $book = Book::find($id);
        $book->book_name = $request->get('book_name');
        $book->author_name = $request->get('author_name');
        $book->section_id = $request->get('section_id');
        $book->genre_id = $request->get('genre_id');
        $book->save();

        $this->ajaxResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $book = Book::find($id);
        $book->is_deleted = 1;
        $book->save();

        $this->ajaxResponse();
    }

    public function ajaxResponse(){
        // $books = Book::where('is_borrowed',0)->orderBy('created_at','DESC')->paginate(10);
        $books = Book::where('is_borrowed',0)->where('is_deleted',0)->orderBy('created_at','DESC')->with(['genre','section'])->paginate(10);


        $data['success'] = 1;
        $data['view'] = view('book.table',compact('books'))->render();
        echo json_encode($data);
    }

    public function search(Request $request)
    {
        $search =  Input::get('search');
        $books = Book::with(['genre','section'])
                            ->where(function($query) use ($search){//group so that borrowed book wont be shown
                                $query->where('book_name', 'LIKE', '%' . $search . '%');
                                $query->orWhere('author_name', 'LIKE', '%' . $search . '%');    
                                $query->orWhereHas('genre',function($q) use ($search){
                                    $q->where('genre_name','LIKE','%'.$search.'%');//check if genre namespace exist 
                                });
                                $query->orWhereHas('section',function($q) use ($search){
                                    $q->where('section_name','LIKE','%'.$search.'%');//check if section name exist
                                });
                            })
                            ->where('is_deleted',0)
                            ->where('is_borrowed',0)
                            ->orderBy('created_at','DESC')->paginate(10);
                            

        // echo json_encode($books);die();

        $books->appends(['search' => $search]);

        if($request->ajax()){
            $data['data'] = $books;
            $data['success'] = 1;
            $data['view'] = view('book.table',compact('books'))->render();
            echo json_encode($data);
        }else{
            return view('book.index',compact('books'));
        }
        
    }

    public function borrow(Request $request)
    {
        $this->validate($request, [
            'book_id' => 'required',
            'client_id' => 'required',
        ]);

        $transaction = new Transaction();
        $transaction->book_id   = $request->book_id;
        $transaction->client_id   = $request->client_id;
        $transaction->borrowed_at   = date('Y-m-d H:i:s');
        $transaction->save();


        $book = Book::find($request->book_id);
        $book->is_borrowed = 1;
        $book->transaction_id = $transaction->id;
        $book->save();

        $this->ajaxResponse();
    }
}
