<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('genre');
        // // $genres = Genre::paginate(15)->orderBy('created_at','DESC')->limit(10)->get();
        // $genres = Genre::orderBy('created_at','DESC')->paginate(10);

        // return view('genre.index',compact('genres'));
    }
}
