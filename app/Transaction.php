<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
   	public function book(){
    	return $this->belongsTo('App\Book','book_id');
    }

    public function client(){
    	return $this->belongsTo('App\Client','client_id');
    }
}
