<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $fillable = [
    	'firstname',
    	'lastname',
    	'email',
    ];

    public function transaction(){
    	return $this->hastMany('App\Transaction');
    }
}
