@extends('layouts.app')

@section('content')
<div class="col-md-10">
	<div class="container-fluid row">
		<div class="col-md-2 row">
			<label class="pull-left">Search Client</label>
			<input type="text" class="pull-left form-control" id="clientSearch" value="">
		</div>
		<button id="add_client" class="btn btn-info pull-right p-2 m-2">Add</button>
	</div>
	<br>
	<div id="client_table_container">
		@include('client.table')
	</div>
</div>


<script src="{{ asset('js/jquery/jquery-3.3.1.js') }}"></script>
<script src="{{ asset('js/jquery/jquery.validation.min.js') }}"></script>
<script>
  
</script>
<script src="{{ asset('js/validation/client-validation.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>


@include('client.create')

@endsection
