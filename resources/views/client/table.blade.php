<table class="table table-responsive table-striped table-bordered">
	<thead class="thead-dark">
		<tr>
			<th>ID</th>
			<th>FIRSTNAME</th>
			<th>LASTNAME</th>
			<th>EMAIL</th>
			<th>DATE CREATE</th>
			<th>ACTION</th>
		</tr>
	</thead>
	<tbody>
		@foreach($clients as $client)
		<tr>
			<td>{{$client->id}}</td>
			<td>{{$client->firstname}}</td>
			<td>{{$client->lastname}}</td>
			<td>{{$client->email}}</td>
			<td>{{$client->created_at}}</td>
			<td>
				<a href="javascript:void(0)" class="btn btn-warning btn-xs edit_client" data-id="{{$client->id}}">
		          	<span class="glyphicon glyphicon-edit"></span> 
		        </a>

		        <a href="javascript:void(0)" class="btn btn-danger btn-xs delete_client" data-id="{{$client->id}}">
		          	<span class="glyphicon glyphicon-trash"></span> 
		        </a>
			</td>
		</tr>
		@endforeach			
	</tbody>
</table>
{{ $clients->links() }}
