
<div class="list-group panel">
    <a href="{{ route('genre') }}" class="list-group-item collapsed  {{ Request::segment(1) === 'genre' ? 'active' : null }} {{ Request::segment(1) === 'genreSearch' ? 'active' : null }}" data-parent="#sidebar"><i class="fa fa-heart"></i> <span class="hidden-sm-down">Genre</span></a>
    <a href="{{ route('section') }}" class="list-group-item collapsed {{ Request::segment(1) === 'section' ? 'active' : null }} {{ Request::segment(1) === 'sectionSearch' ? 'active' : null }}" data-parent="#sidebar"><i class="fa fa-list"></i> <span class="hidden-sm-down">Section</span></a>
    <a href="{{ route('book') }}" class="list-group-item collapsed {{ Request::segment(1) === 'book' ? 'active' : null }} {{ Request::segment(1) === 'bookSearch' ? 'active' : null }}" data-parent="#sidebar"><i class="fa fa-clock-o"></i> <span class="hidden-sm-down">Books</span></a>
    <a href="{{ route('transaction') }}" class="list-group-item collapsed {{ Request::segment(1) === 'transaction' ? 'active' : null }} {{ Request::segment(1) === 'transactionSearch' ? 'active' : null }}" data-parent="#sidebar"><i class="fa fa-th"></i> <span class="hidden-sm-down">Borrowed Book</span></a>
    <a href="{{ route('client') }}" class="list-group-item collapsed {{ Request::segment(1) === 'client' ? 'active' : null }} {{ Request::segment(1) === 'clientSearch' ? 'active' : null }}" data-parent="#sidebar"><i class="fa fa-gear"></i> <span class="hidden-sm-down">Client</span></a>
</div>
