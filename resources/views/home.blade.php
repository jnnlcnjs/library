@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row">
        <main class="col-md-9 col-xs-11 p-l-2 p-t-2">
            <a href="#sidebar" data-toggle="collapse"><i class="fa fa-navicon fa-lg"></i></a>
            <hr>
            <div class="container">
                @yield('content')
            </div>
        </main>
    </div>
</div>



@endsection
