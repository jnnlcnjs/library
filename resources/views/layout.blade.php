<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Laravel 5.7 CRUD Example Tutorial</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
  <style>
  	#sidebar .list-group-item {
    border-radius: 0;
    background-color: #333;
    color: #ccc;
  </style>
</head>
<body>
  <div class="container-fluid row">
    <div class="row">
        <div class="col-md-3 col-xs-1 p-l-0 p-r-0 collapse in" id="sidebar">
			   @include('header.header')
        </div>
        <main class="col-md-9 col-xs-11 p-l-2 p-t-2">
            <a href="#sidebar" data-toggle="collapse"><i class="fa fa-navicon fa-lg"></i></a>
            <hr>
            <div class="container">
    			    @yield('content')
    			</div>
        </main>
    </div>
</div>

  
  <script src="{{ asset('js/app.js') }}" type="text/js"></script>
</body>
</html>