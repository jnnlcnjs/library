@extends('layouts.app')

@section('content')
<div class="col-md-10">
	<div class="container-fluid row">
		<div class="col-md-2 row">
			<label class="pull-left">Search Book</label>
			<input type="text" class="pull-left form-control" id="bookSearch" value="">
		</div>
		<button id="add_book" class="btn btn-info pull-right p-2 m-2">Add</button>
	</div>
	<br>
	<div id="book_table_container">
		@include('book.table')
	</div>
</div>


<script src="{{ asset('js/jquery/jquery-3.3.1.js') }}"></script>
<script src="{{ asset('js/jquery/jquery.validation.min.js') }}"></script>
<script>
  
</script>
<script src="{{ asset('js/validation/book-validation.js') }}"></script>
<script src="{{ asset('js/validation/borrow-validation.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>



@include('book.create')
@include('book.borrow_form')

@endsection
