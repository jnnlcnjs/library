
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<!-- Modal -->

<div class="modal fade" id="borrowBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="bookLabel">Borrow Book</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" id="borrow_form" action="{{ route('bookBorrow') }}">
      <div class="modal-body">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
            </ul>
          </div><br />
        @endif
            <div class="col-md-6" id="book_section">
              <div class="form-group">
                  <label for="name">Book Name:</label>
                  <input type="text" class="form-control" name="book_name" id="book_name"/>
                  <br>

                  <label for="name">Author Name:</label>
                  <input type="text" class="form-control" name="author_name" id="author_name"/>
                  <br>


                  <label for="name">Section:</label><br>
                  <select class="form-control" name="section_id" id="section_id"></select>
                  <br>


                  <label for="name">Genre:</label><br>
                  <select class="form-control" name="genre_id" id="genre_id"></select>

                  <input type="hidden" name="book_id" id="book_id">
              </div>
            </div>
            <div class="col-md-6" id="client_section">
              <div class="form-group">
                  <label for="name">Select Client:</label><br>
                  <select class="form-control" name="client_id" id="client_id"></select>
              </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary closing" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>

    </div>
  </div>
</div>

<script>
$(document).ready(function($){
  $('#client_id').select2({
    placeholder: "Choose client.",
    minimumInputLength: 2,
    width:'100%',
    dropdownParent: $("#borrowBook"),
    ajax: {
        url: '/client/suggest',
        dataType: 'json',
        data: function (params) {
            return {
                q: $.trim(params.term)
            };
        },
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    }
  });

  
});
</script>


