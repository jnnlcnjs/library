<table class="table table-responsive table-striped table-bordered">
	<thead class="thead-dark">
		<tr>
			<th>ID</th>
			<th>NAME</th>
			<th>AUTHOR</th>
			<th>GENRE</th>
			<th>SECTION</th>
			<th>DATE CREATE</th>
			<th>ACTION</th>
		</tr>
	</thead>
	<tbody>
		@foreach($books as $book)
		<tr>
			<td>{{$book->id}}</td>
			<td>{{$book->book_name}}</td>
			<td>{{$book->author_name}}</td>
			<td>{{$book->genre->genre_name}}</td>
			<td>{{$book->section->section_name}}</td>
			<td>{{$book->created_at}}</td>
			<td>
				<a href="javascript:void(0)" class="btn btn-warning btn-xs edit_book" data-id="{{$book->id}}"  data-tooltip="Edit">
		          	<span class="glyphicon glyphicon-edit"></span> 
		        </a>

		        <a href="javascript:void(0)" class="btn btn-danger btn-xs delete_book" data-id="{{$book->id}}">
		          	<span class="glyphicon glyphicon-trash"></span> 
		        </a>

		        <a href="javascript:void(0)" class="btn btn-success btn-xs borrow_book" data-id="{{$book->id}}">
		          	<span class="glyphicon glyphicon-hand-right"></span> 
		        </a>

		        	
			</td>
		</tr>
		@endforeach			
	</tbody>
</table>
{{ $books->links() }}
