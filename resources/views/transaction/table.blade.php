<table class="table table-responsive table-striped table-bordered">
	<thead class="thead-dark">
		<tr>
			<th>ID</th>
			<th>CLIENT NAME</th>
			<th>BOOK NAME</th>
			<th>AUTHOR</th>
			<th>GENRE</th>
			<th>SECTION</th>
			<th>DATE CREATE</th>
			<th>ACTION</th>
		</tr>
	</thead>
	<tbody>


		@foreach($transactions as $transaction)
		<tr>
			<td>{{$transaction->id}}</td>
			<td>{{$transaction->client->firstname." ".$transaction->client->lastname}}</td>
			<td>{{$transaction->book->book_name}}</td>
			<td>{{$transaction->book->author_name}}</td>
			<td>{{$transaction->book->genre->genre_name}}</td>
			<td>{{$transaction->book->section->section_name}}</td>
			<td>{{$transaction->created_at}}</td>
			<td>
				<!-- <a href="javascript:void(0)" class="btn btn-warning btn-xs edit_transactions" data-id="{{$transaction->id}}"  data-tooltip="Edit">
		          	<span class="glyphicon glyphicon-edit"></span> 
		        </a> -->
		        @if($transaction->return_at == NULL)
		        <!-- <a href="javascript:void(0)" class="btn btn-danger btn-xs delete_transactions" data-id="{{$transaction->id}}">
		          	<span class="glyphicon glyphicon-trash"></span> 
		        </a> -->

		        <a href="javascript:void(0)" class="btn btn-success btn-xs return_transactions" data-id="{{$transaction->id}}">
		          	<span class="glyphicon glyphicon-hand-left"></span> 
		        </a>
		        @else
		        	<span>Returned</span>
		        @endif
			</td>
		</tr>
		@endforeach			
	</tbody>
</table>
{{ $transactions->links() }}
