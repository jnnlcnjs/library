@extends('layouts.app')

@section('content')
<div class="col-md-10">
	<div class="container-fluid row">
		<div class="col-md-2 row">
			<label class="pull-left">Search Transaction</label>
			<input type="text" class="pull-left form-control" id="transactionSearch" value="">
		</div>
	</div>
	<br>
	<div id="transaction_table_container">
		@include('transaction.table')
	</div>
</div>


<script src="{{ asset('js/jquery/jquery-3.3.1.js') }}"></script>
<script src="{{ asset('js/jquery/jquery.validation.min.js') }}"></script>
<script>
  
</script>
<script src="{{ asset('js/validation/return-validation.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>


@endsection
