<table class="table table-responsive table-striped table-bordered">
	<thead class="thead-dark">
		<tr>
			<th>ID</th>
			<th>NAME</th>
			<th>DATE CREATE</th>
			<th>ACTION</th>
		</tr>
	</thead>
	<tbody>
		@foreach($sections as $section)
		<tr>
			<td>{{$section->id}}</td>
			<td>{{$section->section_name}}</td>
			<td>{{$section->created_at}}</td>
			<td>
				<a href="javascript:void(0)" class="btn btn-warning btn-xs edit_section" data-id="{{$section->id}}">
		          	<span class="glyphicon glyphicon-edit"></span> 
		        </a>

		        <a href="javascript:void(0)" class="btn btn-danger btn-xs delete_section" data-id="{{$section->id}}">
		          	<span class="glyphicon glyphicon-trash"></span> 
		        </a>
			</td>
		</tr>
		@endforeach			
	</tbody>
</table>
{{ $sections->links() }}
