@extends('layouts.app')

@section('content')
<div class="col-md-10">
	<div class="container-fluid row">
		<div class="col-md-2 row">
			<label class="pull-left">Search Section</label>
			<input type="text" class="pull-left form-control" id="sectionSearch" value="">
		</div>
		<button id="add_section" class="btn btn-info pull-right p-2 m-2">Add</button>
	</div>
	<br>
	<div id="section_table_container">
		@include('section.table')
	</div>
</div>


<script src="{{ asset('js/jquery/jquery-3.3.1.js') }}"></script>
<script src="{{ asset('js/jquery/jquery.validation.min.js') }}"></script>
<script>
  
</script>
<script src="{{ asset('js/validation/section-validation.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>


@include('section.create')

@endsection
