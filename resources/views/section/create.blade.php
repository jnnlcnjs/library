
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<!-- Modal -->

<div class="modal fade" id="addSection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="sectionLabel">Add Section</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" id="section_form" action="{{ route('sectionRequest') }}">
      <div class="modal-body">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
            </ul>
          </div><br />
        @endif
              <div class="form-group">
                  <label for="name">Section Name:</label>
                  <input type="text" class="form-control" name="section_name" id="section_name"/>
                  <input type="hidden" name="id" id="id">
              </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary closing" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>

    </div>
  </div>
</div>



