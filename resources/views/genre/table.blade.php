<table class="table table-responsive table-striped table-bordered">
	<thead class="thead-dark">
		<tr>
			<th>ID</th>
			<th>NAME</th>
			<th>DATE CREATE</th>
			<th>ACTION</th>
		</tr>
	</thead>
	<tbody>
		@foreach($genres as $genre)
		<tr>
			<td>{{$genre->id}}</td>
			<td>{{$genre->genre_name}}</td>
			<td>{{$genre->created_at}}</td>
			<td>
				<a href="javascript:void(0)" class="btn btn-warning btn-xs edit_genre" data-id="{{$genre->id}}">
		          	<span class="glyphicon glyphicon-edit"></span> 
		        </a>

		        <a href="javascript:void(0)" class="btn btn-danger btn-xs delete_genre" data-id="{{$genre->id}}">
		          	<span class="glyphicon glyphicon-trash"></span> 
		        </a>
			</td>
		</tr>
		@endforeach			
	</tbody>
</table>
{{ $genres->links() }}
