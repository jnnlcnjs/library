// Wait for the DOM to be ready
$(document).ready(function($){

  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("#book_form").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      book_name: "required",
      author_name: "required",
      genre_id: {
        required:true,
      },
      section_id: {
        required:true,
      },
    },
    messages: {
      book_name: "Please enter book name.",
      author_name: "Please enter author name.",
      genre_id: "Please select book genre.",
      section: "Please select book section.",
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form,event) {
      event.preventDefault;
      var data = new FormData( form );

      var id = $('#book_form #id').val();

      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        url: id>0?'book/update/'+id:form.action,
        type: 'POST',
  			data: data,
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          success: function(response) {
            if(response.success == 1){
              $('#addBook .closing').trigger('click');
              $('#book_table_container').html(response.view);
            }
          }            
      });
    }
  });

  

});

