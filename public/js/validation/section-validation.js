// Wait for the DOM to be ready
$(document).ready(function($){

  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("#section_form").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      section_name: "required",
    },
    messages: {
      section_name: "Please enter section name.",
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form,event) {
      event.preventDefault;
      var data = new FormData( form );

      var id = $('#section_form #id').val();

      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        url: id>0?'section/update/'+id:form.action,
        type: 'POST',
  			data: data,
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          success: function(response) {
            if(response.success == 1){
              $('#addSection .closing').trigger('click');
              $('#section_table_container').html(response.view);
            }
          }            
      });
    }
  });

  

});

