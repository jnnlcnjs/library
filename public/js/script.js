$(document).ready(function(){

	//GENRE START
	$(document).on('click','#add_genre',function(){
		$('#addGenre').modal('show');
		$('#genre_form #genre_name').val("");
        $('#genre_form #id').val(0);
	});

	var searchGenre = _.debounce(function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

		var data = {
			"search":"as"
		}

	    $.ajax({
	        url: 'genreSearch',
	        type: 'GET',
  			data: "search="+$("#genreSearch").val(),
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          	success: function(response) {
	            if(response.success == 1){
	             	$('#genre_table_container').html(response.view);
	            }
          	}            
	    });
	},600);
	//searching genre
	$(document).on('keyup','#genreSearch',searchGenre);

	//editing genre
	$(document).on('click','.edit_genre',function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

		var id = $(this).attr('data-id');

	    $.ajax({
	        url: '/genre/edit/'+id,
	        type: 'GET',
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          	success: function(response) {
	            if(response){
          			$('#add_genre').trigger('click');
          			$('#genre_form #genre_name').val(response.genre_name);
          			$('#genre_form #id').val(response.id);
	            }
          	}            
	    });
	});

	//deleting genre
	$(document).on('click','.delete_genre',function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

		var id = $(this).attr('data-id');

		swal({
	      	title: "Are you sure you want to delete this item?",
	      	text: "",
	      	icon: "warning",
	      	buttons: [
	        	'No, cancel it!',
	        	'Yes, I am sure!'
	      	],
	      	dangerMode: true,
	    }).then(function(isConfirm) {
	      	if (isConfirm) {
	        	$.ajax({
			        url: '/genre/destroy/'+id,
			        type: 'POST',
		  			async: false,
		  			cache: false,
		  			contentType: false,
		  			processData: false,
		  			dataType: "json",
		          	success: function(response) {
			            if(response.success == 1){
	        				swal("Success", "Item successfully deleted", "success");
              				$('#genre_table_container').html(response.view);
			            }
		          	}            
			    });
	     	}
	    })
	});
	//GENRE END

	//SECTION START
	$(document).on('click','#add_section',function(){
		$('#addSection').modal('show');
		$('#section_form #section_name').val("");
        $('#section_form #id').val(0);
	});

	var searchSection = _.debounce(function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

	    $.ajax({
	        url: 'sectionSearch',
	        type: 'GET',
  			data: "search="+$("#sectionSearch").val(),
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          	success: function(response) {
	            if(response.success == 1){
	             	$('#section_table_container').html(response.view);
	            }
          	}            
	    });
	},600);
	//searching section
	$(document).on('keyup','#sectionSearch',searchSection);

	//editing section
	$(document).on('click','.edit_section',function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

		var id = $(this).attr('data-id');

	    $.ajax({
	        url: '/section/edit/'+id,
	        type: 'GET',
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          	success: function(response) {
	            if(response){
          			$('#add_section').trigger('click');
          			$('#section_form #section_name').val(response.section_name);
          			$('#section_form #id').val(response.id);
	            }
          	}            
	    });
	});

	//deleting section
	$(document).on('click','.delete_section',function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

		var id = $(this).attr('data-id');

		swal({
	      	title: "Are you sure you want to delete this item?",
	      	text: "",
	      	icon: "warning",
	      	buttons: [
	        	'No, cancel it!',
	        	'Yes, I am sure!'
	      	],
	      	dangerMode: true,
	    }).then(function(isConfirm) {
	      	if (isConfirm) {
	        	$.ajax({
			        url: '/section/destroy/'+id,
			        type: 'POST',
		  			async: false,
		  			cache: false,
		  			contentType: false,
		  			processData: false,
		  			dataType: "json",
		          	success: function(response) {
			            if(response.success == 1){
	        				swal("Success", "Item successfully deleted", "success");
              				$('#section_table_container').html(response.view);
			            }
		          	}            
			    });
	     	}
	    })
	});
	//SECTION END


	//CLIENT START
	$(document).on('click','#add_client',function(){
		$('#addClient').modal('show');
		$('#client_form #firstname').val("");
		$('#client_form #lastname').val("");
		$('#client_form #email').val("");
        $('#client_form #id').val(0);
	});

	var searchClient = _.debounce(function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

	    $.ajax({
	        url: 'clientSearch',
	        type: 'GET',
  			data: "search="+$("#clientSearch").val(),
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          	success: function(response) {
	            if(response.success == 1){
	             	$('#client_table_container').html(response.view);
	            }
          	}            
	    });
	},600);
	//searching client
	$(document).on('keyup','#clientSearch',searchClient);

	//editing client
	$(document).on('click','.edit_client',function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

		var id = $(this).attr('data-id');

	    $.ajax({
	        url: '/client/edit/'+id,
	        type: 'GET',
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          	success: function(response) {
	            if(response){
          			$('#add_client').trigger('click');
          			$('#client_form #firstname').val(response.firstname);
          			$('#client_form #lastname').val(response.lastname);
          			$('#client_form #email').val(response.email);
          			$('#client_form #id').val(response.id);
	            }
          	}            
	    });
	});

	//deleting client
	$(document).on('click','.delete_client',function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

		var id = $(this).attr('data-id');

		swal({
	      	title: "Are you sure you want to delete this client?",
	      	text: "",
	      	icon: "warning",
	      	buttons: [
	        	'No, cancel it!',
	        	'Yes, I am sure!'
	      	],
	      	dangerMode: true,
	    }).then(function(isConfirm) {
	      	if (isConfirm) {
	        	$.ajax({
			        url: '/client/destroy/'+id,
			        type: 'POST',
		  			async: false,
		  			cache: false,
		  			contentType: false,
		  			processData: false,
		  			dataType: "json",
		          	success: function(response) {
			            if(response.success == 1){
	        				swal("Success", "Client successfully deleted", "success");
              				$('#client_table_container').html(response.view);
			            }
		          	}            
			    });
	     	}
	    })
	});
	//CLIENT END


	//BOOK START
	$(document).on('click','#add_book',function(){
		$('#addBook').modal('show');
		$('#book_form #book_name').val("");
		$('#book_form #author_name').val("");
		$('#book_form #section_id').html("").val(0).trigger("change");
		$('#book_form #genre_id').html("").val(0).trigger("change");
		$('#book_form #id').val(0);
	});

	var searchBook = _.debounce(function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

	    $.ajax({
	        url: 'bookSearch',
	        type: 'GET',
  			data: "search="+$("#bookSearch").val(),
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          	success: function(response) {
	            if(response.success == 1){
	             	$('#book_table_container').html(response.view);
	            }
          	}            
	    });
	},600);
	//searching book
	$(document).on('keyup','#bookSearch',searchBook);

	//editing book
	$(document).on('click','.edit_book',function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

		var id = $(this).attr('data-id');

	    $.ajax({
	        url: '/book/edit/'+id,
	        type: 'GET',
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          	success: function(response) {
	            if(response){
          			$('#add_book').trigger('click');

          			$('#book_form #book_name').val(response.book_name);
          			$('#book_form #author_name').val(response.author_name);
          			$('#book_form #section_id').html('<option value="'+response.section_id+'">'+response.section.section_name+'</option>');
          			$('#book_form #section_id').val(response.section_id);

          			$('#book_form #genre_id').html('<option value="'+response.genre_id+'">'+response.genre.genre_name+'</option>');
          			$('#book_form #genre_id').val(response.genre_id);

          			$('#book_form #id').val(response.id);
	            }
          	}            
	    });
	});

	//deleting book
	$(document).on('click','.delete_book',function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

		var id = $(this).attr('data-id');

		swal({
	      	title: "Are you sure you want to delete this book?",
	      	text: "",
	      	icon: "warning",
	      	buttons: [
	        	'No, cancel it!',
	        	'Yes, I am sure!'
	      	],
	      	dangerMode: true,
	    }).then(function(isConfirm) {
	      	if (isConfirm) {
	        	$.ajax({
			        url: '/book/destroy/'+id,
			        type: 'POST',
		  			async: false,
		  			cache: false,
		  			contentType: false,
		  			processData: false,
		  			dataType: "json",
		          	success: function(response) {
			            if(response.success == 1){
	        				swal("Success", "Book successfully deleted", "success");
              				$('#book_table_container').html(response.view);
			            }
		          	}            
			    });
	     	}
	    })
	});

	$(document).on('click','.borrow_book',function(){
		$('#borrowBook').modal('show');
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

		var id = $(this).attr('data-id');

	    $.ajax({
	        url: '/book/show/'+id,
	        type: 'GET',
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          	success: function(response) {
	            if(response){
          			$('#borrow_form #book_name').val(response.book_name);
          			$('#borrow_form #author_name').val(response.author_name);
          			$('#borrow_form #section_id').html('<option value="'+response.section_id+'">'+response.section.section_name+'</option>');
          			$('#borrow_form #section_id').val(response.section_id);

          			$('#borrow_form #genre_id').html('<option value="'+response.genre_id+'">'+response.genre.genre_name+'</option>');
          			$('#borrow_form #genre_id').val(response.genre_id);

          			$('#borrow_form #book_id').val(response.id);

          			$('#borrow_form select').attr('readonly',true);
          			$('#borrow_form input').attr('readonly',true);
	            }
          	}            
	    });
	});

	//BOOK END

	///TRANSACTION START
	var searchTransaction = _.debounce(function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});


	    $.ajax({
	        url: 'transactionSearch',
  			data: "search="+$("#transactionSearch").val(),
  			async: false,
  			cache: false,
  			contentType: false,
  			processData: false,
  			dataType: "json",
          	success: function(response) {
	            if(response.success == 1){
	             	$('#transaction_table_container').html(response.view);
	            }
          	}            
	    });
	},600);
	//searching genre
	$(document).on('keyup','#transactionSearch',searchTransaction);

	//deleting genre
	$(document).on('click','.return_transactions',function(){
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	        }
      	});

		var id = $(this).attr('data-id');

		swal({
	      	title: "Are you sure you want to return this book?",
	      	text: "",
	      	icon: "warning",
	      	buttons: [
	        	'No, cancel it!',
	        	'Yes, I am sure!'
	      	],
	      	dangerMode: true,
	    }).then(function(isConfirm) {
	      	if (isConfirm) {
	        	$.ajax({
			        url: '/transaction/returnBooks/'+id,
			        type: 'POST',
		  			async: false,
		  			cache: false,
		  			contentType: false,
		  			processData: false,
		  			dataType: "json",
		          	success: function(response) {
			            if(response.success == 1){
	        				swal("Success", "Book successfully return", "success");
              				$('#transaction_table_container').html(response.view);
			            }
		          	}            
			    });
	     	}
	    })
	});

	///TRANSACTION END

});


